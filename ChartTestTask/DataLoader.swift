import Foundation
protocol DataLoader {
    func loadData() -> Data?
}
/// Загрузчик данных из файла
class BundleDataLoader: DataLoader {
    /// Имя файла
    var fileName: String
    
    init(fileName: String){
        self.fileName = fileName
    }
    
    /// Загрузка данных из файла
    func loadData() -> Data? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                return data
            } catch {
                print("DataLoader error:\(error)")
            }
        }
        return nil
    }
}





