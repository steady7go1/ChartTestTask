import UIKit
class ChartViewController: UIViewController {
    lazy var presenter: ChartPresenter = DependencyContainer.shared.resolve()    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var chartView: ChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.view = self
        presenter.requestData()
        
        setupScrollView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.setupView()
    }
    
    /// Отрисовка слоя с графиком
    func drawChart(_ points: [CGPoint],_ lineWidth: CGFloat, _ lineColor: CGColor){
        chartView.addChartLayer(points,lineWidth,lineColor)
    }
}

extension ChartViewController: UIScrollViewDelegate {
    private func setupScrollView() {
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 10.0
        scrollView.delegate = self
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return chartView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        chartView.updateZoomScale(scrollView.zoomScale)
    }
}


