import Foundation
/// График
struct Chart {
    /// Точки графика
    var points: [Point]
    /// Предельные значения графика
    var limits: Limits
    
    init?(data: ChartData) {
        points = data.points
        limits = Limits(
            minX: points.first?.x ?? 0,
            maxX: points.last?.x ?? 0,
            minY: points.min(by: {$0.y < $1.y})?.y ?? 0,
            maxY: points.max(by: {$0.y < $1.y})?.y ?? 0
        )
    }
    
    /// Предельные значения графика
    struct Limits {
        var minX: Float
        var maxX: Float

        var minY: Float
        var maxY: Float
    }
}

/// Обработчик данных
struct ChartDataParser {
    /// Обработка загруженных данных
    func parse(data: Data) -> ChartData? {
        do {
            let result = try JSONDecoder().decode(ChartData.self, from: data)
            return result
        } catch {
            print("DataParser error:\(error)")
        }
        return nil
    }
}

/// Данные графика
struct ChartData: Decodable {
    var points: [Point]
}
