import UIKit
class ChartView: UIView {
    private var lineWidth: CGFloat = 3    
    private var chartLayer = CAShapeLayer()
    
    /// Обновление толщины линии в соответствии с zoomScale
    func updateZoomScale(_ zoomScale: CGFloat) {
        CATransaction.setValue(kCFBooleanTrue, forKey:kCATransactionDisableActions)
        contentScaleFactor = zoomScale * UIScreen.main.scale
        chartLayer.lineWidth = lineWidth/zoomScale
        CATransaction.commit()
    }
    
    /// Отрисовка слоя с графиком
    func addChartLayer(_ points: [CGPoint],_ lineWidth: CGFloat, _ lineColor: CGColor) {
        let path = UIBezierPath()
        path.lineCapStyle = .round
        path.lineJoinStyle = .round
        
        guard let startPoint = points.first else {return}
        
        path.move(to: startPoint)
        for point in points {
            path.addLine(to: point)
        }
        
        chartLayer.path = path.cgPath
        chartLayer.strokeColor = lineColor
        chartLayer.lineWidth = lineWidth
        chartLayer.fillColor = UIColor.clear.cgColor
        layer.addSublayer(chartLayer)
        
        self.lineWidth = lineWidth
    }
}
