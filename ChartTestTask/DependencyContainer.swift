import Foundation
class DependencyContainer {    
    public static var shared = DependencyContainer()
    private var services: [String: Any] = [:]
    
    func register<T>(interface: T.Type, service: T) {
        let key = String(describing: interface)
        services[key] = service
    }
    
    func resolve<T>() -> T {
        let key = String(describing: T.self)
        return services[key] as! T
    }
}
