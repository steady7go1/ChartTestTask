import UIKit
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Добавляю DataLoader в DI контейнер, потому что приложение в будущем скорее всего будет использоваться метод загрузки данных с сервера и данный подход позволит нам легко поменять метод загрузки и облегчит процесс тестирования
        DependencyContainer.shared.register(interface: DataLoader.self, service: BundleDataLoader(fileName: "points"))
        DependencyContainer.shared.register(interface: ChartPresenter.self, service: ChartPresenter(lineWidth: 3, lineColor: UIColor.black.cgColor))
        
        return true
    }
}



