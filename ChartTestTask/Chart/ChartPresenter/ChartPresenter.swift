import UIKit
class ChartPresenter {
    weak var view: ChartViewController?
    
    private var lineWidth: CGFloat = 3.0
    private var lineColor: CGColor = UIColor.black.cgColor
    private var chart: Chart?
    
    init(lineWidth: CGFloat, lineColor: CGColor) {
        self.lineWidth = lineWidth
        self.lineColor = lineColor
    }
    
    /// Запрос данных
    func requestData() {
        let dataLoader: DataLoader = DependencyContainer.shared.resolve()
        guard let data = dataLoader.loadData() else {return}
        parseData(data)
    }
    
    /// Настройка отображения графика
    func setupView() {
        guard let view = view else {return}        
        let points = calculatePointsForView(view.chartView)
        view.drawChart(points, lineWidth, lineColor)
    }
    
    /// Пересчет координат графика
    private func calculatePointsForView(_ view: UIView) -> [CGPoint] {
        guard let chart = chart else {return [CGPoint]()}
        let chartHeight = CGFloat(chart.limits.maxY - chart.limits.minY)
        let chartWidth = CGFloat(chart.limits.maxX - chart.limits.minX)
        let yScale = chartHeight/view.bounds.height * 3
        let xScale = chartWidth/(view.bounds.width + lineWidth)
        
        return chart.points.map({ point in
            let x = (CGFloat(point.x - chart.limits.minX) / xScale) + lineWidth/2
            let y = (CGFloat(chart.limits.maxY - point.y) / yScale) + view.bounds.height/2 - chartHeight/2/yScale
            return CGPoint(x: x, y: y)
        })
    }

    /// Обработка загруженных данных
    private func parseData(_ data: Data) {
        guard let chartData = ChartDataParser().parse(data: data) else {return}
        chart = Chart(data: chartData)
        setupView()
    }
}
