import Foundation
/// Точка на графике
struct Point : Decodable {
    var x: Float
    var y: Float
}
